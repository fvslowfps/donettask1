﻿using System.Text.Json;
using System.Web;

namespace WeatherApiSimple
{
	public class Program
	{
		public static async Task<int> Main(string[] argv)
		{
			string defaultCity = "Voronezh";
			const string apiKey = "e87810d493fbf9f590b12a8aa87c448c";
			Console.WriteLine("Enter city:");
			string city = Console.ReadLine();
            if (string.IsNullOrEmpty(city))
            {
				city = defaultCity;
            }
            
			// Create a URI (Uniform Resource Identifier)
			var builder = new UriBuilder("https://api.openweathermap.org/data/2.5/weather");
			var queryParameters = HttpUtility.ParseQueryString(builder.Query);
			queryParameters["q"] = city;
			queryParameters["appid"] = apiKey;
			builder.Query = queryParameters.ToString();

			// Perform a request
			Uri uri = builder.Uri;
			var request = new HttpRequestMessage(HttpMethod.Get, uri);
			var client = new HttpClient();
			HttpResponseMessage result = await client.SendAsync(request);

			string jsonContent = await result.Content.ReadAsStringAsync();
			JsonDocument jsonDocument = JsonDocument.Parse(jsonContent);
			double kelvinDegrees = jsonDocument.RootElement
				.GetProperty("main")
				.GetProperty("temp")
				.GetDouble();
			double lon = jsonDocument.RootElement
				.GetProperty("coord")
				.GetProperty("lon")
				.GetDouble();
			double lat = jsonDocument.RootElement
				.GetProperty("coord")
				.GetProperty("lat")
				.GetDouble();
			queryParameters.Remove("q");

			builder = new UriBuilder("https://api.openweathermap.org/data/2.5/onecall");
			queryParameters["lat"] = Convert.ToString(lat);
			queryParameters["lon"] = Convert.ToString(lon);
			builder.Query = queryParameters.ToString();
			uri = builder.Uri;
			request = new HttpRequestMessage(HttpMethod.Get, uri);
			result = await client.SendAsync(request);
			jsonContent = await result.Content.ReadAsStringAsync();
			jsonDocument = JsonDocument.Parse(jsonContent);
			int dateUnixTimeSeconds = jsonDocument.RootElement
				.GetProperty("current")
				.GetProperty("dt")
				.GetInt32();
			int dateSecondsShift = jsonDocument.RootElement
				.GetProperty("timezone_offset")
				.GetInt32();
			string timezone = jsonDocument.RootElement
				.GetProperty("timezone")
				.GetString();
			kelvinDegrees = jsonDocument.RootElement
				.GetProperty("current")
				.GetProperty("temp")
				.GetDouble();

			DateTime offsetUtc = DateTimeOffset.FromUnixTimeSeconds(dateUnixTimeSeconds+ dateSecondsShift).DateTime;
			
			Console.WriteLine("Temp in celsius: {0}, date: {1}, timezone: "+timezone, KelvinToCelsius(kelvinDegrees), offsetUtc);

			return 0;
		}

		private static double KelvinToCelsius(double kelvinDegrees)
		{
			return kelvinDegrees - 273.15;
		}
	}
}